# Healthcare and Life Sciences MLSecOps Considerations

## Introduction

Care delivery decisions are considered as high stakes and predictive models require model interpretability. Clinicians are accountable to understand the relevance of data, estimate its quality, and have ability to investigate the results. Lack of transparency and accountability of predictive models can have severe consequences (Rudin, 2019). Provenance is the lineage and processing history of a data product and the record of the processes that led to it. Provenance is critical to provide chain of custody context and transparency necessary for clinicians to easily interpret and explain model results. Without the appropriate due diligence, care providers are liable to gross negligence in cases where a given patient is harmed or dies.

Different provenance contexts:

-   Data provenance captures the state of input, intermediate, and output data in a pipeline process.

-   Lineage provenance stores the computational activity to create the model, by the storing the instructions used to do the operations on a dataset.

-   Environment provenance collects information about the exact state of the system configuration during data pipelines being executed.

## Regulation Consideration

There are also regulatory considerations with operationally managing models, which span these three domains: 1) research, 2) quality improvement, and 3) clinical operations. Under the "Research" domain, the Institutional Review Board (IRB) plays a large role. Patients are referred to as human subjects. Data is not allowed to be shared for QA or Clinical Operation activities or for other IRB approved projects unless it is an explicitly written in an IRB and approved. One of the governing principals, from a data ethics perspective, is whether consent from human subjects in a study is required or not (Institutional Review Board (IRB) Written Procedures: Guidance for Institutions and IRBs, 2016). If human subjects have not grant consent to use their data outside of a given study, then usage of their data is prohibited and must be isolated from other data sets.

Within the "Quality Improvement" domain clinicians and health policy makers need to make predictions on the likelihood of a diagnosis and the prognosis of a disease to determine the therapy of choice (Steyerberg,2019). The focus is to use methods from evidence-based medicine perspective to develop clinical pathways and standardized treatment protocols. Otherwise, medicine remains much more subjective where it relies on expert knowledge among few practitioners. These predictive models are more targeted towards process improve from a quality improvement perspective.

The "Clinical Operations" domain is where predictive models are more directly associated to care delivery. These are algorithms used to aid, for example, in physiology changes, target vital ranges for treatments, or imaging diagnosis. One of the considerations to keep in mind is the FDA regulations, because models are defined as "Software as a Medical Device" and patient safety plays a large role on how models are used (Proposed Regulatory Framework for Modifications to Artificial Intelligence / Machine Learning Based Software as a Medical Device, 2020). For the most part, it is recommended to treat predictions as "thing of interest" to a clinician versus explicitly claiming an outcome. The rest of the document will break down how these themes are a part of 1) the data science lifecycle, 2) unified logging, and 3) overall platform.

## Data Science Lifecycle

There are four phases a data scientist goes through when developing a predictive model. Exploratory data analysis (EDA) is the starting phase of the work and this is where they are interacting with a data repository (EDW, data lake, etc.). Featuring engineering happens after EDA and the data are transposed or transformed into observations (row) and features (columns). Model development is when models (algorithms) are selected and developed using training and test data sets followed by doing cross validation on a separate sub-set. Model performance is measuring the predictive performance using metrics like confusion matrix, ROC curves, or F1 source. The main complexity a platform needs to address is model provenance and model management.

Model development requires a data scientist to weight the bias vs variance trade off and one basic technique is to split a dataset into thirds. One third is called the training set and it is where the model is trained on. One third is used for testing and it is where models are executed to measure the performance from the training. The last third is validating the model and it is used to verify the generalizability of the model scoring new observations. The provenance of a model is associated to the data set used to build it and when a model is retrained with new data its provenance relationship changes. Depending on the care continuum domain, mentioned in the introduction, will dictate the type of algorithm that should be used from an explainability and interpretability perspective. For example, building a model so it can be used for recommending interventions based on a patient's physiology, then it better to use Bayesian networks instead of neural networks.

Most predictive models are a composite of hyper parameters, features, and an algorithm. Hyper parameters and features are data whereas an algorithm is code. The code is easier to version manage using source code manage tools; however, features and hyper parameters versioning is more complex, because at run time this data represents a state used by the model (basically models are infinite state machines). For versioning the model data requires tracking the historical state transformations of the features engineered as well as the settings for the hyper parameters. Hyper parameters are more direct because they are the settings used by the algorithm. Feature engineering is more involved, because the transformations (or pruning) applied to features are associated to the data set used for training, testing, and validating the model at that time (the data used is a snapshot).

![data science lifecycle](data_science_lifecycle.png)

Feature engineering is where data is transformed into a usable format for model development and each transformation operation applied must be traceable. Conventional data warehousing normalization does not natively support the required data structure for predictive models. Predictive models use a format where a row represents an observation and a column represents a feature. Feature extraction, and dimensional reduction activities are used in the feature engineering phase. Below are a few feature extraction techniques used.

1. Feature date extraction is where parts of a date or timestamp is broken down into base categorical components and / or groupings.

![feature date extraction](feature_date_extraction.png)

1. Feature categorical data transformed into nominal values (1,0).

![feature categorical to nominal values](feature_categorical_to_nominal_values.png)

1. Feature splitting is parsing a string into base categorical elements.

![feature splitting string into categories](feature_splitting_string_to_nominal_values.png)

1. Feature combining is where two more features with weaker signal are combined to create a new feature that may provide a stronger signal.

![feature combination](feature_combining.png)

Dimensional reduction is another area to consider tracking over time, because features provide the input signals needed for models to make future predictions. These three selection techniques are commonly used in the dimensional reduction process; 1) filter method, 2) wrapper method, and 3) embedded method. Filter methods take subsets of data and apply different number of relevant features and measures the interactions between features as it increases the number of samples (Noelia Sánchez-Maroño, 2007). Filter methods rely on the general characteristics of the training data to select features with independence of any predictor (Noelia Sánchez-Maroño, 2007). Filter methods do not incorporate learning (Thomas Navin Lal, 2006).

Wrapper methods involve optimizing a predictor (use of another model to identify features are best predictors) as part of the selection process. This technique is computationally more expensive than filter, and it provide better accuracy on selecting the best features (Noelia Sánchez-Maroño, 2007). This method uses a learning machine to measure the quality of subsets of features without incorporating knowledge about a specific structure (Thomas Navin Lal, 2006). Embedded methods differ from the previous methods in so far as it combines the interaction between selection and learning.

When a applying any of these methods mentioned above features are pruned if they do not carry enough signal. Features with too many missing values are pruned, because they do not carry enough signal to provide useful information. Features with low variance may be pruned for the same reason as too many missing values. High correlation between features may be pruned because they carry similar orthogonal signal for an observation. Backward feature elimination incrementally removes one feature at a time and evaluate the model performance to determine if that feature is pruned. Forward feature construction incrementally adds one feature at a time and evaluates the model performance to identify if the feature is kept.

After a model has been developed and ready to be deploy into operations, there are performance metrics to keep it performant. Metrics like confusion matrix, F1 scores, or ROC curves measure the predictive power relative to its error rate. Part of deployment payload of a model is identifying the error rate thresholds, so a platform understands when to trigger retraining activities. It also provides thresholds for when a model needs to be disabled. Automation to manage models requires coordination with data scientists in order to correctly define the target performance thresholds.

## Unified Log Model

Logging and having a good log model to capture events and provenance of predictive model is a foundational requirement, because of the chain of custody requirements mentioned in the introduction. Unified log model is an approach that provides a way to synchronize a shared, distributed, and fault tolerant method to capture all state change events generated by a data stream being ingested and routed within the ML/AI platform. These logs are designed to have durable event guarantees and strong ordering semantics. As a result, it enables decoupled systems and services from the platform while supporting auditability and handling the metadata mechanics of the data flowing throughout an ecosystem (Kreps, 2014). It is important for all data to be stamped with a lineage id when it first enters the platform and any transformation applied persists that id. This enables replay capability so any output of a model can be reproduced for a specific version of the features and a specific version of the model code. Below is a simplified diagram illustrating the event flow from a pub/sub perspective.

![unified log model](unified_log_model.png)

As a data set or a tuple flows into the eco-system, additional metadata about the payload is recommended besides a lineage id. For example, what service was enacting the read from and write to actions, the source system of the data payload, and the target system the payload will be written too. Timestamps at the nano second is advisable as well, so that post processing can order the events associated to the payload moving within the eco-system. Below is a table with basic meta data elements, at a minimum, which should be captured and associated to data moving inside the platform.

| Attribute              | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| Service description    | Short description or name of the service doing the data work                      |
| Service hostname       | Hostname of the service                                                           |
| Service commit id      | Code repository id generated by the commit for the version of the code being used |
| Service container id   | Container instance id of the running service                                      |
| Service description    | Description or name of source system                                              |
| Source username        | User account used to get the payload from source                                  |
| Source read epoch      | The EPOCH (timestamp) when the payload was read from source                       |
| Target description     | Description or name of source system                                              |
| Target hostname        | Dns or ip of the target host                                                      |
| Target username        | Service account used to access the target host                                    |
| Target write epoch     | The EPOCH (timestamp) when the payload was written to the target system           |
| Message lineage id     | Guid generated when data enters the platform                                      |
| Message description    | Description of the kind/type of message                                           |
| Message format         | Format of the message                                                             |
| Message source         | Original source the message                                                       |
| Message received epoch | The EPOCH (timestamp) when data was first receiving from original source          |
| Message payload        | Payload of the data                                                               |

## Platform

The entire platform should be designed from an event driven perspective, where it does not matter if it is a micro-batch of data or a data stream moving through the eco-system. API layers between services should be used to decouple systems from each other and design a service bus to handle the unified logs from a pub/sub model. Fast data store or a speed layer is meant to be used as an integration layer of data payloads driven by different event frequencies. The data structure requirements will be driven by the features identified by data scientists from their model development activities. The speed layer is the data source feeding predictive models in operations, and it is not designed for predictive model development activities. Long term data store holds all historical data and is updated as data is streaming into the platform. Also, long term data store captures the model outputs too. Predictive models are developed against the long-term data store. The final component to consider is implementing CI/CD automation to build, deploy, and manage models in operation, so that data scientists can quickly iterate on models and quickly push out new versions of those models into operations. The diagram below provides a conceptual view of how these components fit together.

![platform](platform.png)

## Predictive Model CI/CD Automation

This Reference Logical Architecture based on a draft 2.0 version of a platform built at a previous health system. 2.0 incorporate a few lessons learned on the different technologies used from a continuous integration (CI), continuous deployment (CD), and continuous delivery (CD) perspective. One important design principle used is “bring your own model code”, which means a data scientist/ ml engineer can use R, python, or go lang to build a predictive model. Containerization technology enables this to be a reality where an HCLS organization can materialize value from data science activities spanning across different language skills.

The orchestration environment (e.g., Kubernetes) needs a few components installed to support CI/CD pipelines to manage predictive models. CI/CD automation provides an audit trail on changes deployed, and model versioning, which is hugely important with managing models in healthcare. Model provenance is one of the most complex aspect of predictive model management platform. Below in the table is a summary of the technology to use with Kubernetes to support model management from a CI/CD perspective.

<table>
    <tr>
        <td>
            Technology
        </td>
        <td>
            Description
        </td>
    </tr>
    <tr>
        <td>
            Artifactory
        </td>
        <td>
            <ul>
                <li>
                    Container registry to manage certified and verified images
                </li>
                <li>
                    Model artifacts; algorithm code, pickle files, Rdata files, etc.
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            Docker
        </td>
        <td>
            <ul>
                <li>
                    Use containerization technology
                </li>
                <li>
                    Package the libraries required by the model
                </li>
                <li>
                    Environment necessary for the model to run
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            Kubeflow
        </td>
        <td>
            <ul>
                <li>
                    Consistent way of deploying it as a data service
                </li>
                <li>
                    Logs and tracks the instance(s) of model running
                </li>
                <li>
                    Associates instance to data lineage
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            Pachyderm
        </td>
        <td>
            <ul>
                <li>
                    Track the data used for each iteration
                </li>
                <li>
                    Provenance on data sets used during model development
                </li>
                <li>
                    Composite lineage can be made
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            Seldon
        </td>
        <td>
            <ul>
                <li>
                    Outlier detection
                </li>
                <li>
                    Tracking model predictive relevance decay
                </li>
                <li>
                    Ensure proper governance and compliance
                </li>
                <li>
                    Threshold alerts
                </li>
            </ul>
        </td>
    </tr>
</table>

Below is a reference architecture showing the different technologies and services used to facilitate model management with CI/CD automation. The main entry point of a model to get into an environment is by a version control system (Git), which triggers an automation agent to orchestrate the build and deploy process of a model. This is an example of using some toolchain technologies together to support building, testing, deploying, and managing models.ss

![deployment workflow](deployment.png)

### Reference Logical Architecture
<table>
    <tr>
        <td>
            Reference Architecture Component
        </td>
        <td>
            Description
        </td>
    </tr>
    <tr>
        <td>
            A. Development IDE
        </td>
        <td>
            <ul>
                <li>
                    The data science/ ml ops engineer IDE used to develop predictive models
                </li>
                <li>
                    Specifying docker files representing the necessary runtime environment for their models.
                </li>
                <li>
                    CI/CD deployment pipeline automation, driven by domain specific language (DSL) or yet another markup language (YAML) configuration files.
                    <ul>
                        <li>
                            Test cases code
                        </li>
                        <li>
                            Static code analysis
                        </li>
                        <li>
                            Dynamic code analysis
                        </li>
                        <li>
                            Container scanning testing
                        </li>
                        <li>
                            Language linting
                        </li>
                        <li>
                            Build model pickle or R data files
                        </li>
                    </ul>
                </li>
                <li>
                    Predictive model code written in R or python
                </li>
                <li>
                    Model orchestration configurations using K8 or Helm charts
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            B. Version Control System
        </td>
        <td>
            <ul>
                <li>
                    Version control system as the source of truth for all models deployed.
                </li>
                <li>
                    Change audit log and on all changes pushed
                </li>
                <li>
                    Events drive the CI/CD automation agent
                </li>
                <li>
                    CODEOWNERS file to enforce review and approval to code with security or compliance sensitive within the repo
                </li>
                <li>
                    Integrated with automation agent and Static Analysis Security Tests (SAST)/ Dynamic Analysis Security Tests (DAST) testing frameworks
                </li>
                <li>
                    Enforce peer review with code facelifted by branching and pull request to merge into production branch
                </li>
                <li>
                    Strict semantic version with tag support blue/ green deployments
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            C. Automation Agent (Jenkins
        </td>
        <td>
            <ul>
                <li>
                    Automation agent listening to version control system event triggers to facilitated CI/CD pipelines
                </li>
                <li>
                    Orchestrates build, test, verification, and approval process for deployments
                </li>
                <li>
                    Enforces segregation of duties, compliance, and audit logging through automation
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            D. Parameter Data File (Pachyderm)
        </td>
        <td>
            <ul>
                <li>
                    Defined model test, train, and validation pipelines 
                </li>
                <li>
                    Generates parameter data files (e.g., pickle file, Rdata file) from model development or build workflow
                </li>
                <li>
                    Persist data versioning, data pipeline, and data lineage of parameter data compute during developing a model
                </li>
                <li>
                    Track the model parameter (features) provenance
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            E. Container Verification (Twistlock)
        </td>
        <td>
            <ul>
                <li>
                    Container scanning platform to enforce container build policies defined by an HCLS organization
                </li>
                <li>
                    If scanning fails, the Twistlock will tell Automation agent to fail the build process
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            F. Artifacts (Artifactory)
        </td>
        <td>
            <ul>
                <li>
                    Artifact semantic versioning history
                    <ul>
                        <li>
                            Container versions
                        </li>
                        <li>
                            Model pickle/ Rdata model files
                        </li>
                        <li>
                            Algorithm code and hyperparameter settings associated to model data files
                        </li>
                    </ul>
                </li>
                <li>
                    Manages versions used in model orchestration environment
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            G. Network Storage Volume
        </td>
        <td>
            <ul>
                <li>
                    Used as persistent volume for model orchestration environment 
                </li>
                <li>
                    Container instance mounts volume where model pickle/ Rdata data files used by model code
                </li>
                <li>
                    Version of algorithm and hyperparameter settings pushed in the same location as the correct model data file version
                </li>
                <li>
                    This will be a persistent volume used by the model orchestration environment to do persistent volume claims when a container instance is running.
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            H. Model Orchestration (Kubernetes and Kubeflow)
        </td>
        <td>
            <ul>
                <li>
                    Model container orchestration environment managing the different versions of models deployed and resources required to call the model
                </li>
                <li>
                    Able handle to manage predictive models as serverless functions
                </li>
                <li>
                    Supports blue / green deployments of models where these models are managed by an ingress control
                </li>
                <li>
                    Security name spacing and Attribute Based Access Control group policies
                </li>
                <li>
                    Credential vault and/or encryption key management systems can be integrated
                </li>
                <li>
                    Zero trust security principles can be implemented and managed by automation
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            I. Model Performance Management (Seldon)
        </td>
        <td>
            <ul>
                <li>
                    Measures the performance of model instances running
                    <ul>
                        <li>
                            Outlier detection
                        </li>
                        <li>
                            Explanation
                        </li>
                        <li>
                            Bias detection
                        </li>
                    </ul>
                </li>
                <li>
                    Integrated with Kubeflow
                </li>
                <li>
                    Able to measure performance for models written in R or python
                </li>
                <li>
                    Establish thresholds where model performance decay is too high, trigger notifications, and shut down model
                </li>
            </ul>
        </td>
    </tr>
</table>

### Deployment Pipeline
<table>
    <tr>
        <td>
            Deployment Pipeline Component
        </td>
        <td>
            Description
        </td>
    </tr>
    <tr>
        <td>
            1. Pre-commit
        </td>
        <td>
            <ul>
                <li>
                    Pre-commit hooks to trigger linters to enforce code writing consistency
                </li>
                <li>
                    Rapid impact analysis if model performance decays beyond thresholds
                </li>
                <li>
                    Peer review code
                    <ul>
                        <li>
                            model performance
                        </li>
                        <li>
                            test coverage written
                        </li>
                        <li>
                            CI/CD pipeline configurations declared
                        </li>
                        <li>
                            Logic for calculating model data files
                        </li>
                        <li>
                            Performance thresholds defined
                        </li>
                        <li>
                            verify reproducible results is documented with readme files
                        </li>
                    </ul>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            2. Commit
        </td>
        <td>
            <ul>
                <li>
                    SAST on algorithm used
                </li>
                <li>
                    Unit tests triggered
                </li>
                <li>
                    Dependency management
                    <ul>
                        <li>
                            Verify using internal verified libraries stored in Artifactory
                        </li>
                        <li>
                            Check only required libraries are referenced
                        </li>
                        <li>
                            Certify container image specification file only uses an organization's verified images
                        </li>
                    </ul>
                </li>
                <li>
                    Test container specification file 
                    <ul>
                        <li>
                            Only non-root users are used within container
                        </li>
                        <li>
                            File implements security controls based on security guidelines (https://www.nist.gov/publications/application-container-security-guide)
                        </li>
                    </ul>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            3. Acceptance
        </td>
        <td>
            <ul>
                <li>
                    Automation agent listening to version control system event triggers to facilitated acceptance between stages in CI/CD pipelines
                    <ul>
                        <li>
                            Commit and push events trigger CI process to deploy into Dev
                        </li>
                        <li>
                            Linting Dockerfiles, python, R, K8 configs, and Helm Charts
                        </li>
                        <li>
                            Trigger unit tests specified within the repo
                        </li>
                        <li>
                            DAST framework
                        </li>
                        <li>
                            Execute unit test cases
                        </li>
                        <li>
                            Trigger Twistlock scan
                        </li>
                        <li>
                            Build container runtime environment
                        </li>
                        <li>
                            Build python pickle file/ R data file and push to Artifactory to version the predictive model data files
                        </li>
                        <li>
                            Push model payload to persistent volume
                            <ul>
                                <li>
                                    Parameter data file
                                </li>
                                <li>
                                    Algorithm associated to specific version of parameter data file
                                </li>
                                <li>
                                    Hyperparameter settings for algorithm used
                                </li>
                            </ul>
                        </li>
                        <li>
                            Deploy into Dev environment
                        </li>
                        <li>
                            Trigger performance measurement framework
                        </li>
                        <li>
                            Notify mode developer with the results
                        </li>
                    </ul>
                </li>
                <li>
                    Merge pull request events trigger Continuous Deployment into UAT
                    <ul>
                        <li>
                            Promote model payload to UAT name space
                        </li>
                        <li>
                            Run model to score live data
                        </li>
                        <li>
                            User acceptances testers to verify
                            <ul>
                                <li>
                                    If model is for clinical operations, then Clinical or care provider must verify the interpretability and the model’s clinical relevance
                                </li>
                                <li>
                                    If model is for other, then user within that domain verifies model performance and thresholds over time.
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    Tag next semantic version and release to PRD
                    <ul>
                        <li>
                            Promote model payload to PRD name space
                        </li>
                        <li>
                            Enforce semantic version tag to change lineage 
                        </li>
                        <li>
                            Security smoke tests to verify model is compliant
                        </li>
                        <li>
                            Use blue/ green deployment technique to roll on and roll off versions behind an ingress controller
                        </li>
                        <li>
                            Continuously monitor model performance and triage processes when performance decay beyond thresholds
                        </li>
                    </ul>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            4. Algorithm
        </td>
        <td>
            <ul>
                <li>
                    Deploy and promote algorithm between environment name spaces when approvals are given and passed all required test cases 
                </li>
                <li>
                    Associate the algorithm and hyper parameter settings version with parameter data file version
                </li>
                <li>
                    Generate a SHA-256 hash to verify and enforce model version verification when deployed into an environment
                </li>
                <li>
                    Push to persistent storage volume so it is exposed to the orchestration environment when it mounts a persistent volume claim
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            5. Parameter Data
        </td>
        <td>
            <ul>
                <li>
                    Trigger Parameter data pipeline to generate a file based on the training, testing, and validation process 
                </li>
                <li>
                    Associate model parameter data file version with algorithm version a part of the payload
                </li>
                <li>
                    Generate SHA-256 to verify and enforce model parameter data file version verification when deployed into an environment
                </li>
                <li>
                    Incremental processing of model parameter data using massively parallel workloads
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            6. Container Image
        </td>
        <td>
            <ul>
                <li>
                    Trigger container scanning software to verify container complies to HCLS organizational standards
                </li>
                <li>
                    Rejects building the image is tests fail and pushes into Artifactory when the build process succeeds
                </li>
                <li>
                    Runtime version is associated with model payload version 
                </li>
                <li>
                    Incremental changes are saved for the image and tracks changes per layer
                </li>
                <li>
                    Generate SHA-256 to verify and enforce correct runtime environment is used by a specific algorithm version and parameter data file version
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            7. Orchestration
        </td>
        <td>
            <ul>
                <li>
                    Uses Helm charts or K8 configurations to spawn a model container instance mounting a persistent volume claims where model algorithm, and parameter data file exits
                </li>
                <li>
                    Supports rolling on and rolling off different versions using a blue/ green deployment technique where models are behind an ingress controller
                </li>
                <li>
                    Manages the resources requirements specified by the deployment configurations
                </li>
                <li>
                    Orchestrates model runtime with different workload types (e.g., serverless, always on)
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            8. Monitoring
        </td>
        <td>
            <ul>
                <li>
                    Live monitoring of running models deployed
                </li>
                <li>
                    Performance metrics and verification to ensure model prediction are within targeted thresholds
                </li>
                <li>
                    Trigger triage workflow when model does not comply within targeted thresholds
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            9. Notification
        </td>
        <td>
            <ul>
                <li>
                    Instrumented to notified model developers and owners on the performance status
                </li>
                <li>
                    Closes the feedback loop to enable model developers and owners to refine the model quickly
                </li>
                <li>
                    Prompts model developer to refine and redeploy model from feedback
                </li>
            </ul>
        </td>
    </tr>
</table>

## References

+ [Institutional Review Board (IRB) Written Procedures: Guidance for Institutions and IRBs](https://www.hhs.gov/ohrp/regulations-and-policy/requests-for-comments/guidance-for-institutions-and-irbs/index.html)
+ I Heart Logs
+ Filter Methods for Feature Selection
+ [Proposed Regulatory Framework for Modifications to Artificial Intelligence / Machine Learning Based Software as a Medical Device](https://www.fda.gov/files/medical%20devices/published/US-FDA-Artificial-Intelligence-and-Machine-Learning-Discussion-Paper.pdf)
+ [Stop Explaining Black Box Machine Learning Models for High Stakes Decisions and Use Interpretable Models Instead](https://arxiv.org/abs/1811.10154)
+ Clinical Prediction Models
+ Embedded Methods In Feature Extraction
